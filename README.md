# Changelog Vmedis Lite

* Added   : untuk penambahan fitur.
* Changed : untuk perubahan pada menu yang sudah ada atau optimasi.
* Removed : untuk menghapus fitur.
* Fixed   : untuk perbaikan bug atau error.

## [1.0.4.2.1] - 2019-07-16
- **Sistem**
	1. Optimasi dan Perbaikan Aplikasi.
- **Menu Repair Sistem** 
	1. Bisa diakses tanpa perlu login.

## [1.0.2.1] - 2018-09-27
- **Sistem**
	1. Fitur sinkronisasi otomatis data penjualan obat ketika ada koneksi internet.
	2. Fitur cek update versi terbaru.
	3. Optimasi proses konfigurasi domain.

- **Menu Penjualan Barang**
	1. Optimasi pencarian obat supaya lebih cepat.
	2. Implementasi pengurangan nilai stok obat menggunakan metode FIFO.
	
- **Menu Laporan Stok Dan Harga Obat**
	1. Optimasi menu.

- **Menu Laporan Penjualan Obat**
	1. Optimasi menu.
	
- **Menu Laporan Pergantian Shift**
	1. Optimasi menu.